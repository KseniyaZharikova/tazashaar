//
//  Point.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/15/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import MapKit

enum PointType: Int {
    case Bottle = 1
    case Glass = 2
    case Paper = 3
    case Clothing = 4
    case Polyethylene = 5
    case Organic = 6
    case Metal = 7
    case Technics = 8
}

class Point: Object, Mappable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var address: String = ""
    @objc dynamic var descriptionText: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var price: String = ""
    @objc dynamic var departure: String = ""
    @objc dynamic var type: Int = PointType.Bottle.rawValue
    @objc dynamic var workTime: String = ""
    @objc dynamic var images: String = ""
    @objc dynamic var latitude: String = ""
    @objc dynamic var longitude: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        address <- map["address"]
        descriptionText <- map["description"]
        phone <- map["phone"]
        price <- map["price"]
        type <- map["type"]
        workTime <- map["work_time"]
        images <- map["images"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        departure <- map["departure"]
    }
    
    func getLocation() -> CLLocationCoordinate2D?{
        if let lat = Double(latitude), let lon = Double(longitude){
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
        
        return nil
    }
    
    func getColor() -> UIColor{
        switch type {
        case PointType.Bottle.rawValue:
            return UIColor(hex: 0xce7eec)
        case PointType.Glass.rawValue:
            return UIColor(hex: 0xfed429)
        case PointType.Paper.rawValue:
            return UIColor(hex: 0x61e6cd)
        case PointType.Clothing.rawValue:
            return UIColor(hex: 0xfa4c7e)
        case PointType.Polyethylene.rawValue:
            return UIColor(hex: 0x1678d1)
        case PointType.Organic.rawValue:
            return UIColor(hex: 0x6bb957)
        case PointType.Metal.rawValue:
            return UIColor(hex: 0xf58233)
        case PointType.Technics.rawValue:
            return UIColor(hex: 0x454853)
        default:
            return UIColor.blue
        }

    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let point = object as? Point {
            return self.id == point.id
        } else {
            return false
        }
    }
}

