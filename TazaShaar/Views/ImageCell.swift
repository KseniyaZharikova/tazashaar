//
//  ImageCell.swift
//  TazaShaar
//
//  Created by Kseniya on 5/2/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class ImageCell: UICollectionViewCell {
    
    var imageUIView = ImageUIView()
    static let identity = "ImageCell"
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        contentView.addSubview(imageUIView)
        
        imageUIView.snp_makeConstraints { make in
            make.right.left.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.equalToSuperview()
           
        }
        
    }
    
    func setImage(urlImage: String){
       imageUIView.setImage(urlImage: urlImage)
    }
    
    func setupSize(_ frame: CGRect) {
        imageUIView.snp_makeConstraints { make in
            make.right.left.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.size.equalTo(CGSize(width: frame.width, height: frame.height))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
}
