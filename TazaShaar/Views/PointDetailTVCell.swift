//
//  PointDetailTVCell.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/18/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit


class PointDetailTVCell: UITableViewCell {
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var deparrtureLabel: UILabel!
    @IBOutlet weak var collectionViewImage: UICollectionView!
    
    
    @IBOutlet weak var descriptionLabel: UILabel!
    var imagesArray:[String] = []
    
    
    
    private var point : Point = Point() {
        didSet{
            locationLabel.text = point.address.trimmingCharacters(in: CharacterSet.newlines)
            phoneNumberLabel.numberOfLines = 0
            phoneNumberLabel.text = point.phone.trimmingCharacters(in: CharacterSet.newlines)
            scheduleLabel.text = point.workTime.trimmingCharacters(in: CharacterSet.newlines)
            priceLabel.text = point.price.trimmingCharacters(in: CharacterSet.newlines)
            deparrtureLabel.text = "Выезд: \(point.departure.trimmingCharacters(in: CharacterSet.newlines))"
            descriptionLabel.text = point.descriptionText.trimmingCharacters(in: CharacterSet.newlines)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionViewImage.delegate = self
        collectionViewImage.dataSource = self
        let collectionLayout = collectionViewImage.collectionViewLayout as! UICollectionViewFlowLayout
        if #available(iOS 10.0, *) {
            collectionLayout.itemSize = UICollectionViewFlowLayout.automaticSize
        }
        collectionLayout.estimatedItemSize =  CGSize(width: collectionViewImage.frame.width, height: 100)
        
        
        collectionViewImage.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.identity)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupWith(point: Point){
        self.point = point
    }
    
}

extension PointDetailTVCell: UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let images = point.images
        imagesArray = images.components(separatedBy: ",")
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.identity, for: indexPath) as! ImageCell
        cell.setupSize(collectionView.frame)
        cell.setImage(urlImage: imagesArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size =  collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

