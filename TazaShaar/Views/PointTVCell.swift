//
//  PointTVCell.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/18/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit

class PointTVCell: UITableViewCell {

    
    @IBOutlet weak var pointNameLabel: UILabel!
    @IBOutlet weak var departureLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    private var point : Point = Point() {
        didSet{
            pointNameLabel.text = point.name.trimmingCharacters(in: CharacterSet.newlines)
            locationLabel.text = point.address.trimmingCharacters(in: CharacterSet.newlines)
            
            phoneNumberLabel.text = point.phone.trimmingCharacters(in: CharacterSet.newlines)
            scheduleLabel.text = point.workTime.trimmingCharacters(in: CharacterSet.newlines)
            priceLabel.text = point.price.trimmingCharacters(in: CharacterSet.newlines)
            departureLabel.text = "\(NSLocalizedString("Выезд: ",comment:""))\(point.departure.trimmingCharacters(in: CharacterSet.newlines))"
            descriptionLabel.text = point.descriptionText.trimmingCharacters(in: CharacterSet.newlines)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(point: Point){
        self.point = point
    }
}
