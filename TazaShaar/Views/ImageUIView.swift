//
//  ImageUIView.swift
//  TazaShaar
//
//  Created by Kseniya on 5/6/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//


import UIKit
import SnapKit
import Kingfisher

class ImageUIView: UIView {
    
    var imagePoint = UIImageView()
    static let identity = "ImageCell"
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        addSubview(imagePoint)
        imagePoint.contentMode = .scaleAspectFit

        imagePoint.snp_makeConstraints { make in
            make.right.left.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.equalToSuperview()
          
        }
        
    }
    
    func setImage(urlImage: String){
        imagePoint.kf.setImage(with: URL(string:urlImage.replacingOccurrences(of: " ", with: "")), placeholder: UIImage(named: "nophoto"))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
}
