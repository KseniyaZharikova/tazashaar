//
//  ViewController.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/15/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import FirebaseDatabase
import ObjectMapper
import Pulley

class SplashVC: UIViewController {
    var needUpdate = false
    override func viewDidLoad() {
        super.viewDidLoad()
       
        loadData ()
    }
    
    
    func loadData () {
        Database.database().reference().child(NSLocalizedString("reception_points",comment:"")).observe(.value) { (snapshot) in
            
            var points = [Point]()
            
            for child in snapshot.children{
                if let childrenSnapshot = child as? DataSnapshot, let json = childrenSnapshot.value as? [String: Any], let point = Mapper<Point>().map(JSON: json) {
                    points.append(point)
                }
            }
            
            CachingManager.instance.savePoints(points: points)
            self.perform(#selector(self.openMap), with: nil, afterDelay: 1.0)
        }
        
        self.perform(#selector(self.openMap), with: nil, afterDelay: 4.0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.perform(#selector(startRotateAnimation), with: nil, afterDelay: 0.2)
    }
    
    
    //    @objc func startRotateAnimation(){
    //        let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
    //        rotation.toValue = Double.pi * 2
    //        rotation.duration = 8
    //        rotation.isCumulative = true
    //        rotation.repeatCount = Float.greatestFiniteMagnitude
    //        logoImageView.layer.add(rotation, forKey: "rotationAnimation")
    //    }
    
    @objc func openMap(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RootVC") as! RootVC
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
}

