//
//  MapVC.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/15/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import RealmSwift
import MapKit
import CoreLocation

protocol MapVCDelegate {
    func didSelectAnnotationPoint(point: Point)
}

class MapVC: UIViewController {
    
    @IBOutlet weak var mapview: MKMapView!
    
    private var notificationToken: NotificationToken? = nil
    private var points = CachingManager.instance.getPoints()
    private let locationManager = CLLocationManager()
    var delegate : MapVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.isUpdateAvailable()
        mapview.delegate = self
        mapview.showsUserLocation = true
        locationManager.startUpdatingLocation()
        showAllPoints()
        self.perform(#selector(checkLocation), with: nil, afterDelay: 1.0)
    }
    
    @objc func checkLocation(){
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    func setCenterMap(parameterscoordinate: CLLocationCoordinate2D, onePoint : Bool) {
        mapview.centerCoordinate = parameterscoordinate
        var pointCentered = parameterscoordinate
        var regionRadius: CLLocationDistance
        var coordinateRegion = MKCoordinateRegion()
        
        if onePoint {
            pointCentered.latitude =  pointCentered.latitude - 0.004
            regionRadius  = 500
            coordinateRegion = MKCoordinateRegion(center: pointCentered,
                                                  latitudinalMeters: regionRadius * 3.0, longitudinalMeters: regionRadius * 3.0)
        } else {
            pointCentered.latitude =  pointCentered.latitude - 0.05
            regionRadius = 3000
            coordinateRegion = MKCoordinateRegion(center: pointCentered,
                                                  latitudinalMeters: regionRadius * 5.0, longitudinalMeters: regionRadius * 5.0)
            
        }
        
        mapview.setRegion(coordinateRegion, animated: true)
    }
    
    @IBAction func showCurrentLocation(_ sender: Any) {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            setCenterMap(parameterscoordinate: mapview.userLocation.coordinate,onePoint: true)
        }else{
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)! as URL
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        }
    }
    
    func showAllPoints(){
        points = CachingManager.instance.getPoints()
        notificationToken = points.observe { [weak self] (changes: RealmCollectionChange) in
            
            guard let self = self else { return }
            
            switch changes {
            case .error(let error):
                print("error: \(error)")
            default :
                var annotations = [MKAnnotation]()
                for point in self.points{
                    if let location = point.getLocation(){
                        let annotation = PointAnnotation()
                        annotation.coordinate = location
                        annotation.point = point
                        annotations.append(annotation)
                    }
                }
                
                self.mapview.removeAnnotations(self.mapview.annotations)
                self.mapview.addAnnotations(annotations)
                self.mapview.showAnnotations(annotations, animated: true)
                self.setCenterMap(parameterscoordinate: CLLocationCoordinate2D(latitude: 42.8748635, longitude: 74.6048324), onePoint:false)
            }
        }
        
    }
    
    func showPointsByType(type: PointType){
        points = CachingManager.instance.getPointsByType(type: type)
        notificationToken = points.observe { [weak self] (changes: RealmCollectionChange) in
            
            guard let self = self else { return }
            
            switch changes {
            case .error(let error):
                print("error: \(error)")
            default :
                var annotations = [MKAnnotation]()
                for point in self.points{
                    if let location = point.getLocation(){
                        let annotation = PointAnnotation()
                        annotation.coordinate = location
                        annotation.point = point
                        annotations.append(annotation)
                    }
                }
                
                self.mapview.removeAnnotations(self.mapview.annotations)
                self.mapview.addAnnotations(annotations)
                self.mapview.showAnnotations(annotations, animated: true)
                self.setCenterMap(parameterscoordinate: CLLocationCoordinate2D(latitude: 42.8748635, longitude: 74.6048324), onePoint:false)
            }
        }
    }
    
    func showPoint(point: Point){
        if let location = point.getLocation(){
            let annotation = PointAnnotation()
            annotation.coordinate = location
            annotation.point = point
            self.mapview.removeAnnotations(self.mapview.annotations)
            self.mapview.addAnnotation(annotation)
            self.mapview.showAnnotations([annotation], animated: true)
            self.setCenterMap(parameterscoordinate: location, onePoint: true)
        }
    }
    
    func isUpdateAvailable() {
        let url = URL(string: "https://itunes.apple.com/lookup?bundleId=com.tazashaar.prod")
        let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error ) in
            guard error == nil else {
                print("returned error")
                return
            }
            guard let content = data else {
                print("No data")
                return
            }
            
            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                return
            }
            
            
            if let result = (json["results"] as? [Any])?.first as? [String: Any], let version =
                result["version"] as? String {
                
                self.shooseIfNeedUpdated(isNew: currentVersion != version)
                
            }
        }
        
        task.resume()
        
    }
    
    private func shooseIfNeedUpdated(isNew: Bool){
        
        if isNew {
            DispatchQueue.main.async {
                  self.showAlert(title:NSLocalizedString("Внимание!",comment:""), message: NSLocalizedString("Ваша версия приложения устарела, вам нужно обновиться.",comment:""))
            }
          
        }
    }
    
    func showAlert(title:String,message:String){
        let checkUrl: String = "itms-apps://itunes.apple.com/app/apple-store/id1453696558"
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title:NSLocalizedString("Обновить",comment:""), style: UIAlertAction.Style.default) {
            UIAlertAction in
            UIApplication.shared.open(URL(string: checkUrl)!)
        }
        let cancelAction = UIAlertAction(title:NSLocalizedString("Отмена",comment:"") , style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension MapVC : MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{
            return nil;
        }else{
            let view = CustomAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            if let custom = annotation as? PointAnnotation{
                view.point = custom.point
            }
            
            return view
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        mapView.showAnnotations([view.annotation!], animated: true)
        
        if let delegate = self.delegate, let customAnnotation = view.annotation as? PointAnnotation{
            delegate.didSelectAnnotationPoint(point: customAnnotation.point)
        }
    }
}


class PointAnnotation: MKPointAnnotation {
    var point = Point()
}


class CustomAnnotationView : MKAnnotationView{
    
    var point = Point()
    
    open lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.clear
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.white.cgColor
        self.addSubview(imageView)
        return imageView
    }()
    
    open lazy var innerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 5
        imageView.backgroundColor = UIColor.clear
        self.addSubview(imageView)
        return imageView
    }()
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        imageView.frame = bounds
        innerImageView.frame = CGRect(origin: CGPoint(x: bounds.origin.x + 5, y: bounds.origin.y + 5), size: CGSize(width: 10, height: 10))
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        configure(annotation: annotation)
    }
    
    func configure(annotation: MKAnnotation?){
        
        guard let annotation = annotation as? PointAnnotation else {
            return
        }
        
        frame = CGRect(origin: frame.origin, size: CGSize(width: 20, height: 20))
        imageView.layer.borderColor = annotation.point.getColor().cgColor
        innerImageView.backgroundColor = annotation.point.getColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let annotation = object as? CustomAnnotationView {
            return point.id == annotation.point.id
        } else {
            return false
        }
    }
}
