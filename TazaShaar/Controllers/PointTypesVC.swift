//
//  BottomSheetVC.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/15/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import Pulley

protocol PointTypesVCDelegate {
    func didSelectType(type: PointType)
    func checkNetWork()
}

class PointTypesVC: UIViewController {

    @IBOutlet weak var typesStackView: UIStackView!
    var delegate: PointTypesVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showTypes)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       showTypes()
    }
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
       showTypes()
    }
  
    
    @objc func showTypes(){
        if let drawer = self.parent as? PulleyViewController{
            drawer.setDrawerPosition(position: PulleyPosition.partiallyRevealed, animated: false)
        }
        if let delegate = self.delegate {
            delegate.checkNetWork()
        }
    }
    
    @IBAction func showTypePoints(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didSelectType(type: PointType(rawValue: sender.tag)!)
        }
    }

}

extension PointTypesVC: PulleyDrawerViewControllerDelegate {
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        typesStackView.isHidden = drawer.drawerPosition == PulleyPosition.collapsed
    }
    
    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
    {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        return 80.0 + bottomSafeArea
    }
    
    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
    {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        let  height = UIDevice.isIphone5Screen() ? 280.0 + bottomSafeArea : 320.0 + bottomSafeArea
        return height
    }
    
    func supportedDrawerPositions() -> [PulleyPosition] {
        return [PulleyPosition.partiallyRevealed, PulleyPosition.collapsed]
    }
}
