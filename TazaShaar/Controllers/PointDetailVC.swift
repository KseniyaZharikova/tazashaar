//
//  PointDetailVC.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/18/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import Pulley

protocol PointDetailVCDelegate {
    func didBack(vc: PointDetailVC)
}

class PointDetailVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var point = Point()
    var delegate : PointDetailVCDelegate?
    var isFromMap = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.titleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showTypes)))
    }
    
    
    func setupUI()  {
        tableView.dataSource = self
        tableView.register(UINib(nibName: "PointDetailTVCell", bundle: nil), forCellReuseIdentifier: "PointDetailTVCell")
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView(frame: .zero)
        
        titleLabel.text = point.name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showTypes()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        showTypes()
        setupUI()
    }
    
    @objc func showTypes(){
        if let drawer = self.parent as? PulleyViewController, drawer.drawerPosition == PulleyPosition.collapsed{
            drawer.setDrawerPosition(position: PulleyPosition.partiallyRevealed, animated: true)
        }
    }
    
    @IBAction func back(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.didBack(vc: self)
        }
    }
}

extension PointDetailVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PointDetailTVCell", for: indexPath) as! PointDetailTVCell
        
        cell.setupWith(point: point)
        
        return cell
    }
}

extension PointDetailVC: PulleyDrawerViewControllerDelegate {
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        tableView.isHidden = drawer.drawerPosition == PulleyPosition.collapsed
    }
    
    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
    {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        return 60.0 + bottomSafeArea
    }
    
    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
    {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        let  height = UIDevice.isIphone5Screen() ? 300.0 + bottomSafeArea : 320.0 + bottomSafeArea
        return height
    }
    
    func supportedDrawerPositions() -> [PulleyPosition] {
        return [PulleyPosition.partiallyRevealed, PulleyPosition.collapsed]
    }
}
