//
//  RootVC.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/16/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import Pulley

class RootVC: PulleyViewController {
    
    let mapVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapVC") as! MapVC
    let pointTypesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PointTypesVC") as! PointTypesVC
    let typePointsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TypePointsVC") as! TypePointsVC
    
    override func viewDidLoad() {
        //        super.viewDidLoad()
        
        mapVC.delegate = self
        pointTypesVC.delegate = self
        typePointsVC.delegate = self
        
        self.setPrimaryContentViewController(controller: mapVC, animated: true)
        self.setDrawerContentViewController(controller: pointTypesVC, animated: false)
        
        
    }
    
    
}

extension RootVC : MapVCDelegate{
    func didSelectAnnotationPoint(point: Point) {
        mapVC.showPoint(point: point)
        
        let pointDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PointDetailVC") as! PointDetailVC
        pointDetailVC.point = point
        pointDetailVC.delegate = self
        pointDetailVC.isFromMap = true
        self.setDrawerContentViewController(controller: pointDetailVC, animated: false)
        self.setDrawerPosition(position: PulleyPosition.collapsed, animated: true)
    }
}

extension RootVC : PointTypesVCDelegate{
    
    func checkNetWork() {
        if !NetworkUtils.isConnectedToInternet(){
            
            if CachingManager.instance.getPoints().isEmpty {
                let alert = UIAlertController(title: NSLocalizedString ("Внимание", comment: ""), message: NSLocalizedString ("Отсутствует интернет соединение, невозможно получить данные пунктов приема отходов", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Закрыть",comment:""), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
        
        
    }
    
    func didSelectType(type: PointType) {
        guard !CachingManager.instance.getPointsByType(type: type).isEmpty else {
            let alert1 = UIAlertController(title: NSLocalizedString ("Внимание", comment: "") , message: NSLocalizedString ("В данной категории к сожалению нет пунктов приема", comment: ""), preferredStyle: .alert)
            
            alert1.addAction(UIAlertAction(title: NSLocalizedString("Закрыть",comment:""), style: .cancel, handler: nil))
            self.present(alert1, animated: true, completion: nil)
            
            return
        }
        
        mapVC.showPointsByType(type: type)
        
        typePointsVC.type = type.rawValue
        self.setDrawerContentViewController(controller: typePointsVC, animated: false)
        self.setDrawerPosition(position: PulleyPosition.collapsed, animated: true)
    }
}

extension RootVC : TypePointsVCDelegate{
    func didSelectBack() {
        mapVC.showAllPoints()
        self.setDrawerContentViewController(controller: pointTypesVC, animated: false)
        self.setDrawerPosition(position: PulleyPosition.collapsed, animated: true)
    }
    
    func didSelectPoint(point: Point) {
        mapVC.showPoint(point: point)
        
        let pointDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PointDetailVC") as! PointDetailVC
        pointDetailVC.point = point
        pointDetailVC.delegate = self
        self.setDrawerContentViewController(controller: pointDetailVC, animated: false)
        self.setDrawerPosition(position: PulleyPosition.collapsed, animated: true)
    }
}

extension RootVC : PointDetailVCDelegate{
    func didBack(vc: PointDetailVC) {
        if vc.isFromMap{
            mapVC.showAllPoints()
            self.setDrawerContentViewController(controller: pointTypesVC, animated: false)
            self.setDrawerPosition(position: PulleyPosition.collapsed, animated: true)
        }else{
            mapVC.showPointsByType(type: PointType(rawValue: vc.point.type)!)
            
            typePointsVC.type = vc.point.type
            self.setDrawerContentViewController(controller: typePointsVC, animated: false)
            self.setDrawerPosition(position: PulleyPosition.collapsed, animated: true)
        }
    }
}
