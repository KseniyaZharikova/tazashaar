//
//  TypePointsTVC.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/16/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import Pulley
import RealmSwift

protocol TypePointsVCDelegate {
    func didSelectBack()
    func didSelectPoint(point: Point)
}

class TypePointsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    private var points = CachingManager.instance.getPointsByType(type: PointType.Bottle)
    private var notificationToken: NotificationToken? = nil
    var type = PointType.Bottle.rawValue
    var delegate : TypePointsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "PointTVCell", bundle: nil), forCellReuseIdentifier: "PointTVCell")
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView(frame: .zero)
        
        self.titleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showTypes)))
    }
    
   
    override func viewDidAppear(_ animated: Bool) {
        showTypes()
    }
    
    @objc func showTypes(){
        if let drawer = self.parent as? PulleyViewController, drawer.drawerPosition == PulleyPosition.collapsed{
            drawer.setDrawerPosition(position: PulleyPosition.partiallyRevealed, animated: true)
        }
    }
    
    
    func reloadData(){
        switch type {
        case PointType.Bottle.rawValue:
            titleLabel.text = NSLocalizedString("Прием пластика:",comment:"")
        case PointType.Glass.rawValue:
            titleLabel.text = NSLocalizedString("Прием стекла:",comment:"")
        case PointType.Paper.rawValue:
            titleLabel.text = NSLocalizedString("Прием бумаги:",comment:"")
        case PointType.Clothing.rawValue:
            titleLabel.text = NSLocalizedString("Прием одежды:",comment:"")
        case PointType.Polyethylene.rawValue:
            titleLabel.text = NSLocalizedString("Прием полиэтилена:",comment:"")
        case PointType.Organic.rawValue:
            titleLabel.text = NSLocalizedString("Прием органики:",comment:"")
        case PointType.Metal.rawValue:
            titleLabel.text = NSLocalizedString("Прием металла:",comment:"")
        case PointType.Technics.rawValue:
            titleLabel.text = NSLocalizedString("Прием техники:",comment:"")
        default:
            titleLabel.text = ""
        }
        
        showPointsByType()
    }
    
    func showPointsByType(){
        points = CachingManager.instance.getPointsByType(type: PointType(rawValue: type)!)
        notificationToken = points.observe { [weak self] (changes: RealmCollectionChange) in
            
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
                break
            case .update(_, let deletions, let insertions, let modifications):
                
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .none)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .none)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .none)
                tableView.endUpdates()
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTypes()
        reloadData()
    }
    
    @IBAction func back(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.didSelectBack()
        }
    }
}

extension TypePointsVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return points.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PointTVCell", for: indexPath) as! PointTVCell
        
        cell.setupWith(point: points[indexPath.row])
        
        return cell
    }
}

extension TypePointsVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let delegate = self.delegate{
            delegate.didSelectPoint(point: points[indexPath.row])
        }
    }
}

extension TypePointsVC: PulleyDrawerViewControllerDelegate {
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        tableView.isHidden = drawer.drawerPosition == PulleyPosition.collapsed
    }
    
    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
    {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        return 60.0 + bottomSafeArea
    }
    
    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat
    {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        return 320.0 + bottomSafeArea
    }
    
    func supportedDrawerPositions() -> [PulleyPosition] {
        return [PulleyPosition.collapsed, PulleyPosition.open, PulleyPosition.partiallyRevealed]
    }
}
