//
//  Devices.swift
//  TazaShaar
//
//  Created by Kseniya on 4/20/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import Foundation
import UIKit

public extension UIDevice {
    private static var identifier: String {
        
        // App is running on a simulated device
        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
            return simulatorModelIdentifier
        } else {
            // App is running on a device
            var systemInfo = utsname()
            uname(&systemInfo)
            
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            
            return machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8 , value != 0 else { return identifier }
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
        }
    }
    
    internal static var device: Device {
        
        switch identifier {
        case "iPhone1,1":  // iPhone Original - A1203
            return .iPhoneOriginal
        case "iPhone1,2":  // iPhone 3G       - A1241 A1324
            return .iPhone3G
        case "iPhone2,1":  // iPhone 3GS      - A1303 A1325
            return .iPhone3GS
        case "iPhone3,1",  // iPhone 4        - A1332
        "iPhone3,1",  // iPhone 4        - A1332
        "iPhone3,2":  // iPhone 4        - A1349
            return .iPhone4
        case "iPhone4,1":  // iPhone 4s       - A1387 A1431
            return .iPhone4S
        case "iPhone5,1",  // iPhone 5        - A1428, A1429
        "iPhone5,2":  // iPhone 5        - A1429, A1442
            return .iPhone5
        case "iPhone5,3",  // iPhone 5c       - A1532, A1456
        "iPhone5,4":  // iPhone 5c       - A1507, A1526, A1529, A1516
            return .iPhone5c
        case "iPhone6,1",  // iPhone 5s       - A1533, A1453
        "iPhone6,2":  // iPhone 5s       - A1457, A1528, A1530, A1518
            return .iPhone5s
        case "iPhone7,2":  // iPhone 6        - A1549, A1586, A1589
            return .iPhone6
        case "iPhone7,1":  // iPhone 6 plus   - A1522, A1524, A1593
            return .iPhone6P
        case "iPhone8,1":  // iPhone 6s       - A1633, A1688, A1700, A1691
            return .iPhone6S
        case "iPhone8,2":  // iPhone 6s plus  - A1634, A1687, A1699, A1690
            return .iPhone6SP
        case "iPhone8,4":  // iPhone SE       - A1662, A1723, A1724
            return .iPhoneSE
        case "iPhone9,1",   // iPhone 7        - A1660, A1779, A1780
        "iPhone9,3":        // iPhone 7        - A1778
            return .iPhone7
        case "iPhone9,2",   // iPhone 7 plus   - A1661, A1785, A1786
        "iPhone9,4":        // iPhone 7 plus   - A1784
            return .iPhone7P
        case "iPhone10,1",  // iPhone 8        - A1863, A1906
        "iPhone10,1":       // iPhone 8        - A1905
            return .iPhone8
        case "iPhone10,2",  // iPhone 8 plus   - A1864, A1898
        "iPhone10,5":       // iPhone 8 plus   - A1897
            return .iPhone8P
        case "iPhone10,3",  // iPhone X        - A1865, A1902
        "iPhone10,6":       // iPhone X        - A1901
            return .iPhoneX
        case "iPad1,1":     // iPad Original   - A1219 A1337
            return .iPadOriginal
        case "iPad2,1",     // iPad 2          - A1395
        "iPad2,2",          // iPad 2          - A1396
        "iPad2,3",          // iPad 2          - A1397
        "iPad2,4":          // iPad 2          - A1395
            return .iPad2
        case "iPad2,5",     // iPad Mini       - A1432
        "iPad2,6",          // iPad Mini       - A1454
        "iPad2,7":          // iPad Mini       - A1455
            return .iPadMini
        case "iPad3,1",     // iPad 3rd Gen    - A1416
        "iPad3,2",          // iPad 3rd Gen    - A1403
        "iPad3,3":          // iPad 3rd Gen    - A1430
            return .iPad3G
        case "iPad3,4",     // iPad 4th Gen    - A1458
        "iPad3,5",          // iPad 4th Gen    - A1459
        "iPad3,6":          // iPad 4th Gen    - A1460
            return .iPad4G
        case "iPad4,1",     // iPad Air        - A1474
        "iPad4,2",          // iPad Air        - A1475
        "iPad4,3":          // iPad Air        - A1476
            return .iPadAir
        case "iPad4,4",     // iPad Mini 2     - A1489
        "iPad4,5",          // iPad Mini 2     - A1490
        "iPad4,6":          // iPad Mini 2     - A1491
            return .iPadMini2
        case "iPad4,7",     // iPad Mini 3     - A1599
        "iPad4,8",          // iPad Mini 3     - A1600
        "iPad4,9":          // iPad Mini 3     - A1601
            return .iPadMini3
        case "iPad5,1",     // iPad Mini 4     - A1538
        "iPad5,2":          // iPad Mini 4     - A1550
            return .iPadMini4
        case "iPad5,3",     // iPad Air 2      - A1566
        "iPad5,4":          // iPad Air 2      - A1567
            return .iPadAir2
        case "iPad6,11",    // iPad (5g)       - A1822
        "iPad6,12":         // iPad (5g)       - A1823
            return .iPad5G
        case "iPad6,3",     // iPad Pro 9.7    - A1673
        "iPad6,4":          // iPad Pro 9.7    - A1674
            return .iPadPro9_7
        case "iPad6,7",     // iPad Pro 12.9   - A1584
        "iPad6,8",          // iPad Pro 12.9   - A1652
        "iPad7,1",          // iPad Pro 12.9   - A1670
        "iPad7,2":          // iPad Pro 12.9   - A1671
            return .iPadPro12_9
        case "iPad7,3",     // iPad Pro 10.5   - A1701
        "iPad7,4":          // iPad Pro 10.5   - A1709
            return .iPadPro10_5
        default:
            return .other
        }
    }
    
    static func isIphone5Screen() -> Bool {
        return device == .iPhone5 || device == .iPhone5s || device == .iPhone5c
    }
    static func isIphone4Screen() -> Bool {
        return device == .iPhone4S
    }
    
    static func isPlusScreen() -> Bool {
        return device == .iPhone6P || device == .iPhone6SP || device == .iPhone7P || device == .iPhone8P || device == .iPhoneX
    }
}
enum Device {
    
    // MARK: iPhone
    
    case iPhoneOriginal
    case iPhone3G
    case iPhone3GS
    case iPhone4
    case iPhone4S
    case iPhone5
    case iPhone5c
    case iPhone5s
    case iPhoneSE
    case iPhone6
    case iPhone6P
    case iPhone6S
    case iPhone6SP
    case iPhone7
    case iPhone7P
    case iPhone8
    case iPhone8P
    case iPhoneX
    
    // MARK: iPad
    
    case iPadOriginal
    case iPad2
    case iPadMini
    case iPad3G
    case iPad4G
    case iPadAir
    case iPadMini2
    case iPadMini3
    case iPadMini4
    case iPadAir2
    case iPad5G
    case iPadPro9_7
    case iPadPro12_9
    case iPadPro10_5
    
    // MARK: Misc
    
    case other
}
