//
//  Repository.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/15/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import RealmSwift

class CachingManager {
    static let instance = CachingManager()
    private let realm = try! Realm()

    func clearAll(){
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func savePoints(points: [Point]){
        do {
            try realm.write {
                realm.add(points, update: true)
            }
        } catch (let error){
            print("FireBaseError: \(error.localizedDescription)")
        }
    }
    
    func getPoints() -> Results<Point> {
        return realm.objects(Point.self)
    }
   
    func getPointsByType(type: PointType) -> Results<Point> {
        let predicate = NSPredicate(format: "type = \(type.rawValue)")
        return realm.objects(Point.self).filter(predicate)
    }
}
