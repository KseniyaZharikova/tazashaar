//
//  NetworkUtils.swift
//  TazaShaar
//
//  Created by Kseniya on 4/20/19.
//  Copyright © 2019 Kseniya. All rights reserved.
//
import Foundation
import Alamofire

class NetworkUtils {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
