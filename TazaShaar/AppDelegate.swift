//
//  AppDelegate.swift
//  TazaShaar
//
//  Created by Azamat Kushmanov on 2/15/19.
//  Copyright © 2019 Azamat Kushmanov. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupRealm()
        FirebaseApp.configure()
        
        return true
    }
    
    func setupRealm() {
        
        var realmConfiguration = Realm.Configuration.defaultConfiguration
        realmConfiguration.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = realmConfiguration
        
        let realm = try! Realm()
        print("realm path: \(String(describing: realm.configuration.fileURL?.absoluteURL))")
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


}

